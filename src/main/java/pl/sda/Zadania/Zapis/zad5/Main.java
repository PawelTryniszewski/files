package pl.sda.Zadania.Zapis.zad5;

import java.io.*;
import java.util.Scanner;

/**
 * Stwórz klasę formularz. Formularz reprezentuje odpowiedzi których udzielił użytkownik.
 * Aplikacja ma po uruchomieniu rozpocząć od zadawania pytań użytkownikowi.
 * Po wpisaniu danych(odpowiedzi na pytania) zapisz te odpowiedzi do pliku (output_form.txt)
 * w odpowiednim formacie (przez format mamy na myśli zawartość - np. oddziel pytania i odpowiedzi tak,
 * aby byly pisane w nowych liniach). Poproś użytkownika o: wiek, wzrost, płeć (kobieta/mezczyzna),
 * zarobki i zadaj dwa dodatkowe pytania. Po czynności zamknij plik i program.
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String odpowiedz;
        String wiek = "Ile masz lat?";
        String wzrost = "Ile masz cm?";
        String plec = "Kobieta czy meżczyzna?";
        String zarobki = "Ile zarabiasz?";
        String hobby = "Jakie jest Twoje hobby?";
        String czyLubisz = "Czy lubisz robic zadania w weekend?";
        PrintWriter zapispliku=null;
        try {
            zapispliku = new PrintWriter(new FileWriter("Formularz.txt",false));
            System.out.println(wiek);
            zapispliku.println(wiek);
            odpowiedz=scanner.nextLine();
            zapispliku.println(odpowiedz);
            zapispliku.flush();

            System.out.println(wzrost);
            zapispliku.println(wzrost);
            odpowiedz=scanner.nextLine();
            zapispliku.println(odpowiedz);
            zapispliku.flush();

            System.out.println(plec);
            zapispliku.println(plec);
            odpowiedz=scanner.nextLine();
            zapispliku.println(odpowiedz);
            zapispliku.flush();

            System.out.println(zarobki);
            zapispliku.println(zarobki);
            odpowiedz=scanner.nextLine();
            zapispliku.println(odpowiedz);
            zapispliku.flush();

            System.out.println(hobby);
            zapispliku.println(hobby);
            odpowiedz=scanner.nextLine();
            zapispliku.println(odpowiedz);
            zapispliku.flush();

            System.out.println(czyLubisz);
            zapispliku.println(czyLubisz);
            odpowiedz=scanner.nextLine();
            zapispliku.println(odpowiedz);
            zapispliku.flush();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
