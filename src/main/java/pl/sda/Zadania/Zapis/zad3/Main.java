package pl.sda.Zadania.Zapis.zad3;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *  Należy napisać aplikację która w pętli while czyta ze Scannera wejście użytkownika z konsoli,
 *  a następnie linia po linii wypisuje tekst do pliku 'plik03.txt'. Aplikacja ma się zamykać
 *  po wpisaniu przez użytkownika komendy "quit".
 *
 * Pętla powinna być w try a nie try w pętli. - w przeciwnym razie w pętli
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisuj tekst. Zeby zakonczyc wpisywanie napisz /quit/");
        PrintWriter zapisPliku = null;
        try {
            zapisPliku = new PrintWriter(new FileWriter("pilk03.txt", false));
            String tekstOdUzytkownika;
            do {
                tekstOdUzytkownika = scanner.nextLine();
                if (!tekstOdUzytkownika.equals("quit")) {
                    zapisPliku.println(tekstOdUzytkownika);
                    zapisPliku.flush();
                }

            } while (!tekstOdUzytkownika.equals("quit"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
