package pl.sda.Zadania.Zapis.zad4;

import java.io.File;
import java.util.Scanner;

/**
 * Napisz program, a w tym programie wczytaj jedną linię ze skanera od użytkownika.
 * Poproś użytkownika o 'adres pliku'. Po wpisaniu zweryfikuj czy wybrany plik/katalog istnieje,
 * czy jest plikiem/katalogiem, jaki jest jego: rozmiar, czas ostatniej modyfikacji i czy mamy
 * prawa do odczytu/zapisu do tego pliku/katalogu.
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Podaj sciezke do pliku");
        String sciezka = scanner.nextLine();
        File szukanyPlik = new File(sciezka);
        System.out.println(szukanyPlik.exists());
        System.out.println(szukanyPlik.isDirectory());
        System.out.println(szukanyPlik.length());
        System.out.println(szukanyPlik.lastModified());
        System.out.println(szukanyPlik.canRead());
        System.out.println(szukanyPlik.canWrite());

    }
}
