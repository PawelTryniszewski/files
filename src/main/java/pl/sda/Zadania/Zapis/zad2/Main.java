package pl.sda.Zadania.Zapis.zad2;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Napisz aplikację w której:
 *  - wczytaj od użytkownika jedną linię tekstu
 *  - po wczytaniu linii otwórz plik o nazwie 'output_2.txt'
 *  - zapisz do pliku linię pobraną od użytkownika
 *  - zamknij plik
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz tekst");
        String tekstOdUzytkownika = scanner.nextLine();
        try {
            PrintWriter zapisPliku = new PrintWriter("plik02.txt");
            zapisPliku.println(tekstOdUzytkownika);
            zapisPliku.flush();
            zapisPliku.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
