package pl.sda.Zadania.Zapis.zad7;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FormularzPlec {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String odpowiedz;
        String wiek = "Ile masz lat?";
        String wzrost = "Ile masz cm?";
        String plec = "Jaka jest Twoja plec? M/K";
        String zarobki = "Ile zarabiasz?";
        String hobby = "Jakie jest Twoje hobby?";
        String czyLubisz = "Czy lubisz robic zadania w weekend?";
        PrintWriter zapispliku = null;


        try {
            zapispliku = new PrintWriter(new FileWriter("Formularz2.txt", false));
            String odpowiedzClose;
            do {
                System.out.println(plec);
                zapispliku.println(plec);
                odpowiedz = scanner.nextLine().toLowerCase();
                zapispliku.println(odpowiedz);
                zapispliku.flush();

                if (odpowiedz.equals("m")) {

                    System.out.println(wiek);
                    zapispliku.println(wiek);
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();

                    System.out.println(wzrost);
                    zapispliku.println(wzrost);
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();


                    System.out.println(zarobki);
                    zapispliku.println(zarobki);
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();

                    System.out.println(hobby);
                    zapispliku.println(hobby);
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();

                    System.out.println(czyLubisz);
                    zapispliku.println(czyLubisz);
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();
                }else if (odpowiedz.equals("k")){

                    System.out.println("Jaki wiek?");
                    zapispliku.println("Ile masz Lat");
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();

                    System.out.println("Czy male jest piekne?");
                    zapispliku.println("Czy male jest piekne?");
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();


                    System.out.println("Czy jestes zadowolona ze swoich zarobkow?");
                    zapispliku.println("Czy jestes zadowolona ze swoich zarobkow?");
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();

                    System.out.println("Powiedz, czym sie interesujesz?");
                    zapispliku.println("Powiedz, czym sie interesujesz?");
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();

                    System.out.println("Czy jestes inna niz wszystkie?");
                    zapispliku.println("Czy jestes inna niz wszystkie?");
                    odpowiedz = scanner.nextLine();
                    zapispliku.println(odpowiedz);
                    zapispliku.flush();

                }else{
                    System.out.println("Pomyliles sie przy wpisywaniu jednej litery! Gratulacje!");
                }


                System.out.println("Czy chcesz wypelnic formularz jeszcze raz? t/n");
                odpowiedzClose = scanner.nextLine().toLowerCase();
                if (odpowiedzClose.equals("t")) {
                    System.out.println();
                    System.out.println("Wypelnij formularz");
                    System.out.println();
                    zapispliku.println();
                    zapispliku.println("NOWY FORMULARZ");

                } else if (odpowiedzClose.equals("n")) {
                    zapispliku.flush();
                    zapispliku.close();
                    System.out.println("Dziekuje");
                }else{
                    System.out.println("Pomyliles sie przy wpisywaniu jednej litery! Gratulacje!\n" +
                            "W nagrode wypelnisz formularz jeszcze raz");
                }


            } while (!odpowiedzClose.equals("n"));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
