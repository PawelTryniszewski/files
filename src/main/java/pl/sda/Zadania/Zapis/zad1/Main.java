package pl.sda.Zadania.Zapis.zad1;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Napisz aplikację która otwiera plik 'output_1.txt', zapisuje do niego "Hello World!" a następnie zamyka plik.
 */

public class Main {
    public static void main(String[] args) {
        try {
            PrintWriter zapisPliku = new PrintWriter("plik01.txt");
            zapisPliku.println("Hello World!");
            zapisPliku.flush();
            zapisPliku.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
