package pl.sda.Zadania.Zapis.zad6;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Zmodyfikuj kod z zadania 5. w powyższym zadaniu wypełnialiśmy 1 raz formularz.
 * Tym razem otwieraj aplikację i dopisuj dane z wypełnionych formularzy na końcu pliku.
 * Po zakończeniu wypełniania formularza nie kończ pracy programu tylko zadaj użytkownikowi
 * pytanie o to czy chce wypełnić formularz ponownie, czy chce zakończyć program. Zakończ
 * program jeśli użytkownik wpisze że nie chce wypełniać kolejnego formularza.
 */

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String odpowiedz;
        String wiek = "Ile masz lat?";
        String wzrost = "Ile masz cm?";
        String plec = "Kobieta czy meżczyzna?";
        String zarobki = "Ile zarabiasz?";
        String hobby = "Jakie jest Twoje hobby?";
        String czyLubisz = "Czy lubisz robic zadania w weekend?";
        PrintWriter zapispliku = null;


        try {
            zapispliku = new PrintWriter(new FileWriter("Formularz1.txt", false));
            String odpowiedzClose;
            do {

                System.out.println(wiek);
                zapispliku.println(wiek);
                odpowiedz = scanner.nextLine();
                zapispliku.println(odpowiedz);
                zapispliku.flush();

                System.out.println(wzrost);
                zapispliku.println(wzrost);
                odpowiedz = scanner.nextLine();
                zapispliku.println(odpowiedz);
                zapispliku.flush();

                System.out.println(plec);
                zapispliku.println(plec);
                odpowiedz = scanner.nextLine();
                zapispliku.println(odpowiedz);
                zapispliku.flush();

                System.out.println(zarobki);
                zapispliku.println(zarobki);
                odpowiedz = scanner.nextLine();
                zapispliku.println(odpowiedz);
                zapispliku.flush();

                System.out.println(hobby);
                zapispliku.println(hobby);
                odpowiedz = scanner.nextLine();
                zapispliku.println(odpowiedz);
                zapispliku.flush();

                System.out.println(czyLubisz);
                zapispliku.println(czyLubisz);
                odpowiedz = scanner.nextLine();
                zapispliku.println(odpowiedz);
                zapispliku.flush();

                System.out.println("Czy chcesz wypelnic formularz jeszcze raz? t/n");
                odpowiedzClose = scanner.nextLine().toLowerCase();
                if (!odpowiedzClose.equals("n")) {
                    System.out.println();
                    System.out.println("Wypelnij formularz");
                    System.out.println();
                    zapispliku.println();
                    zapispliku.println("NOWY FORMULARZ");

                }else if (odpowiedzClose.equals("n")){
                    System.out.println("Dziekuje");
                }


            } while (!odpowiedzClose.equals("n"));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
