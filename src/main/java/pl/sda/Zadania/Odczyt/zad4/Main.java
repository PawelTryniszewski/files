package pl.sda.Zadania.Odczyt.zad4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Napisz aplikację która czyta plik 'Formularz.txt' i wypisuje zawartość formularzy.
 */

public class Main {
    public static void main(String[] args) {
        File odczytPliku = new File("formularz.txt");
        try {
            Scanner scanner = new Scanner((odczytPliku));

            while (scanner.hasNextLine()) {
                String tekst =scanner.nextLine();
                System.out.println(tekst);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
