package pl.sda.Zadania.Odczyt.zad1;

import java.io.*;

/**
 * Stwórz aplikację która służy do odczytywania danych z plików które zapisaliśmy,
 * stwórz main'a w którym sprawdzaj że w katalogu w którym uruchomiłeś program jest
 * plik o nazwie 'plik01.txt' który posiada treść "Hello World!". Jeśli pliku nie
 * ma lub posiada inną zawartość napisz odpowiedni komunikat.
 */

public class Main {
    public static void main(String[] args) {
        File odczytPliku = new File("plik01.txt");
        if (odczytPliku.exists()) {
            System.out.println("plik istnieje");
            try {
                BufferedReader reader = new BufferedReader(new FileReader(odczytPliku));
                String tekstPliku = reader.readLine();
                if (tekstPliku.toLowerCase().contains("hello worl")) ;{
                    System.out.println("Zawiera szukany napis");
                }
                if (!tekstPliku.toLowerCase().contains("hello world!")){
                    System.out.println("Nie zawiera szukanego napisu.");
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            System.out.println("Plik nie istnieje");
        }
    }

}

