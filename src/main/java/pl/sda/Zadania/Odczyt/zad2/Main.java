package pl.sda.Zadania.Odczyt.zad2;

import java.io.*;

/**
 *
 * Stwórz aplikację która odczyta plik 'plik02.txt' i wczyta z niego linię,
 * a następnie wypisze ją na ekran: 1 raz "toUpperCase", 1 raz "toLowerCase", 1 raz "trim".
 */
public class Main {
    public static void main(String[] args) {
        File odczytPliku = new File("plik02.txt");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(odczytPliku));
            String liniaZPliku= reader.readLine();
            System.out.println(liniaZPliku.toUpperCase());
            System.out.println(liniaZPliku.toLowerCase());
            System.out.println(liniaZPliku.trim());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
