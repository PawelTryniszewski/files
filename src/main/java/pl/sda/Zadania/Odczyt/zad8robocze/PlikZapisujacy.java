package pl.sda.Zadania.Odczyt.zad8robocze;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class PlikZapisujacy {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String komendaWyjscia = null;

        do {
            String nazwaPliku;
            System.out.println("Podaj nazwe pliku. Bedziesz zapisywać w nowoutworzonym pliku o podanej nazwie");
            nazwaPliku = new Scanner(System.in).nextLine() + ".txt";
            System.out.println("Wpisuj tekst. Zeby zakonczyc wpisywanie napisz /quit/");
            PrintWriter zapisPliku = null;
            try {
                zapisPliku = new PrintWriter(new FileWriter(nazwaPliku, false));
                String tekstOdUzytkownika;
                do {
                    tekstOdUzytkownika = new Scanner(System.in).nextLine();
                    if (!tekstOdUzytkownika.equals("quit")) {
                        zapisPliku.println(tekstOdUzytkownika);
                        zapisPliku.flush();
                        System.out.println("Zapisano '" + tekstOdUzytkownika + "'");
                    }

                } while (!tekstOdUzytkownika.equals("quit"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Skonczyles zapisywac w pliku " + nazwaPliku);
            System.out.println("Zeby zakonczyc prace wpisz quit, żeby utworzyc nowy plik wpisz nowy");
            komendaWyjscia = scanner.next().toLowerCase();
            nazwaPliku = null;


        } while (!komendaWyjscia.equals("quit"));
    }
}
