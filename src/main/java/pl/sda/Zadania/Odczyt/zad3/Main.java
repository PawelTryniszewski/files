package pl.sda.Zadania.Odczyt.zad3;

import java.io.*;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Stwórz aplikację która czyta plik 'plik03.txt' i zlicza słowa i linie w tym pliku.
 */
public class Main {
    public static void main(String[] args) throws NoSuchElementException {
        File odczytPliku = new File("pilk03.txt");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(odczytPliku));
            Scanner scanner = new Scanner(new FileReader(odczytPliku));

            String tekstPliku;
            int liczbaLinii = 0;
            int liczbaSlow = 0;

            tekstPliku = reader.readLine();

            do {
                System.out.println(tekstPliku);
                liczbaLinii++;
                while (scanner.hasNext()) {
                    scanner.next();
                    liczbaSlow++;
                }
            } while (null != (tekstPliku = reader.readLine()));


            System.out.printf("Liczba lini: %d\n", liczbaLinii);
            System.out.printf("Łączna liczba słów: %d", liczbaSlow);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
