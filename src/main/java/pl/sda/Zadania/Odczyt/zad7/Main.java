package pl.sda.Zadania.Odczyt.zad7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Stwórz aplikację która pyta użytkownika o ścieżkę do pliku i wyszukiwane słowo.
 * Następnie sprawdź czy podany plik istnieje, a jeśli istnieje to zlicz wystąpienia
 * podanego słowa w pliku.
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj sciezke do pliku");
        String sciezka = scanner.nextLine();
        System.out.println("Podaj szukane słowo");
        String szukaneSlowo = scanner.nextLine();
        File szukanyPlik = new File(sciezka);
        int licznikSlowa=0;
        if (szukanyPlik.exists()) {
            try {
                Scanner scann = new Scanner(new FileReader(szukanyPlik));
                while(scann.hasNextLine()){
                    String tekst =scann.nextLine();
                    if (tekst.toLowerCase().contains(szukaneSlowo)){
                        licznikSlowa++;
                    }
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("Slowo '%s' wystąpilo %d razy",szukaneSlowo,licznikSlowa);
    }

}
