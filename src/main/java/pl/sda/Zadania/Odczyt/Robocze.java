package pl.sda.Zadania.Odczyt;

import java.io.*;

public class Robocze {
    public static void main(String[] args) {
        int countSpace = 0;
        int nextLine = 0;
        String linia;

        try {
            BufferedReader reader = new BufferedReader(new FileReader("pilk03.txt"));

            while ((linia = reader.readLine()) != null)
                if (linia != null) {
                    nextLine ++;
                    System.out.println(linia);
                    for (int i = 0; i < linia.length(); i++) {
                        if (linia.charAt(i) == ' ') {
                            countSpace ++;
                        }
                    }
                }

            // To rozwiązanie jest dobre, tylko do czasu aż nie ma pustego akapitu
            System.out.println("Liczba wierszy: " + nextLine);
            System.out.printf("Liczba wyrazów: %d", nextLine + countSpace);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
