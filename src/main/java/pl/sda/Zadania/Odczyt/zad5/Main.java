package pl.sda.Zadania.Odczyt.zad5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Napisz aplikację która czyta plik 'Formularz2.txt' i wypisuje ilość wypełnionych formularzy.
 */

public class Main {
    public static void main(String[] args) {
        File odczytPliku = new File("Formularz2.txt");
        int iloscFormularzy=0;
        try {
            Scanner scanner = new Scanner(odczytPliku);
            String tekst;
            while(scanner.hasNextLine()){
                tekst =scanner.nextLine();
                if (tekst.toLowerCase().contains("ile masz lat")){
                    iloscFormularzy++;
                }
            }
            System.out.printf("Ilosc wypelnionych formularzy: %d",iloscFormularzy);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
